import {Injectable} from '@angular/core';
import {ToDo} from './todo.model';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {map, filter, catchError} from 'rxjs/operators'; // lettable operators are new since RxJS 5.5

import * as cuid from 'cuid';

// const backendUrl = 'http://localhost:3456/todos';
// const backendUrl = 'http://localhost:4001/api/todos';
// const backendUrl = 'https://jba-todo-1.now.sh/api/todos';
const backendUrl = location.origin + '/api/todos';

@Injectable()
export class ToDoService {

  private clientId: string;

  constructor(private http: HttpClient) {
    this.clientId = localStorage.getItem('JBA-CLIENT-ID');

    if (!this.clientId) {
      this.clientId = cuid();
      localStorage.setItem('JBA-CLIENT-ID', this.clientId);
    }
  }

  getTodos(): Observable<ToDo[]> {
    return this.http.get(backendUrl, { headers: {'xx-jba-client-id': this.clientId} })
      .pipe(
        map(
          (res: any) => res.data.map((r) => {
            const todo = new ToDo(r.title);
            todo.completed = r.completed;
            todo.id = r.id;
            return todo;
          })),
        catchError(this.handleError)
      );
  }

  saveTodo(todo: ToDo): Observable<ToDo> {

    return this.http.post(backendUrl, todo, { headers: {'xx-jba-client-id': this.clientId} })
      .pipe(
        map((response: any) => {
          const data = response.data;
          const persistedToDo = new ToDo(data.title);
          persistedToDo.completed = data.completed;
          persistedToDo.id = data.id;
          return persistedToDo;
        }),
        catchError(this.handleError)
      );
  }

  updateTodo(todo: ToDo): Observable<ToDo> {
    return this.http.put(`${backendUrl}/${todo.id}`, todo, { headers: {'xx-jba-client-id': this.clientId} })
      .pipe(
        catchError(this.handleError)
      );
  }

  deleteTodo(todo: ToDo): Observable<any> {
    return this.http.delete(`${backendUrl}/${todo.id}`, { headers: {'xx-jba-client-id': this.clientId} });
  }

  private handleError(error: any) {
    const errMsg = error.message || 'Server error';
    console.error(errMsg);
    return ErrorObservable.create(errMsg);
  }

}

