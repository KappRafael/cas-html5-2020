import styles from './About.module.scss';

function About() {
  return (
    <div className={styles.comic}>
      <h1>About</h1>
      <p>This is the About component.</p>
    </div>
  );
}

export default About;
