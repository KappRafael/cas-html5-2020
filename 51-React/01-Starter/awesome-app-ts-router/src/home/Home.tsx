import { Greeter } from './Greeter';

export function Home() {
    return (
        <div>
            <h1>Home</h1>
            <Greeter message="Hello"/>
        </div>
    );
}

export default Home;
