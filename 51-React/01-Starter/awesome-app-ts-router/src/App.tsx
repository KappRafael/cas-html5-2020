import { lazy, Suspense } from 'react';
import { NavLink, Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import './App.scss';

// Note: code-splitting with dynamic import() is optional. The componets could also be imported statically.
const About = lazy(() => import('./about/About'));
const Home = lazy(() => import('./home/Home'));

function App() {
  return (
      <div className="App">
        <Router>
          <header className="App-header">
            <ul>
              <li>
                <NavLink to="/" exact>Home</NavLink>
              </li>
              <li>
                <NavLink to="/about" exact>About</NavLink>
              </li>
            </ul>
          </header>
          <div className="App-body">
            <Suspense fallback={<h3>Loading ...</h3>}>
              <Switch>
                <Route path="/about">
                  <About/>
                </Route>
                <Route path="/">
                  <Home/>
                </Route>
              </Switch>
            </Suspense>
          </div>
        </Router>
      </div>
  );
}

export default App;
