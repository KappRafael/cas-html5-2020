export async function fetchPictureUrl(){
  const response = await fetch(`https://api.thecatapi.com/v1/images/search`).then(sleep(1000));
  const data = await response.json();
  const url = data[0].url;
  console.log('URL', url);
  return url;
}

function sleep (msecs) {
  return results => new Promise(resolve => setTimeout(() => resolve(results), msecs))
}
