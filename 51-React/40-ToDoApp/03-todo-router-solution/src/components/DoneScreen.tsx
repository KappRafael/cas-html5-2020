import { useEffect, useState } from 'react';
import { loadToDos, storeToDos } from '../persistence';
import { ToDoList } from './ToDoList';
import { ToDo } from '../model/todo';

export default function DoneScreen() {
  const [todos, setTodos] = useState<ToDo[]>([]);

  useEffect(() => {
    const todos = loadToDos();
    updateToDos(todos);
  }, []);

  function removeToDo(toDo: ToDo) {
    const todos = loadToDos();
    const newToDos = todos.filter(t => t.id !== toDo.id);
    storeToDos(newToDos);

    updateToDos(newToDos);
  }

  function updateToDos(updatedToDos: ToDo[]) {
    const pendingToDos = updatedToDos.filter(t => t.completed);
    setTodos(pendingToDos);
  }

  return (
    <div>
      <div className="main">
        <ToDoList todos={todos} onRemoveToDo={removeToDo} />
      </div>
    </div>
  );
}
