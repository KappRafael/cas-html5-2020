import { useState, useEffect } from 'react';
import { loadToDos, storeToDos } from '../persistence';
import { ToDo } from '../model/todo';
import { NewToDoForm } from '../components/NewToDoForm';
import { ToDoList } from '../components/ToDoList';

export default function ToDoScreen() {
  const [todos, setTodos] = useState<ToDo[]>([]);

  useEffect(() => {
    const todos = loadToDos();
    updateToDos(todos);
  }, []);

  function addToDo(title: string) {
    const newToDos = [...todos, { id: Math.random().toString(), title: title, completed: false }];
    setTodos(newToDos);
    storeToDos(newToDos);
  }

  function completeToDo(toDo: ToDo) {
    const todos = loadToDos();
    const updatedToDos = todos.map(t => (t.id !== toDo.id ? t : { ...toDo, completed: true }));
    storeToDos(updatedToDos);

    updateToDos(updatedToDos);
  }

  function updateToDos(updatedToDos: ToDo[]) {
    const pendingToDos = updatedToDos.filter(t => !t.completed);
    setTodos(pendingToDos);
  }

  return (
    <section className="todoapp">
      <NewToDoForm onAddToDo={addToDo} />

      <div className="main">
        {/* eslint-disable-next-line react/jsx-no-undef */}
        <ToDoList todos={todos} onRemoveToDo={completeToDo} />
      </div>
    </section>
  );
}
