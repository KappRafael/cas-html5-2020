import * as actions from './notificationReducer';

export function showMessage(message){
    return {type: actions.showMessage.type, payload: message};
}
