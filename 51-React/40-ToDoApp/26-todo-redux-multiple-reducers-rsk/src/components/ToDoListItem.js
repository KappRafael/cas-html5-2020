import React from 'react';

function ToDoListItem({ todo, onRemoveToDo }) {
  return (
    <li key={todo.id}>
      {todo.title}
      <button onClick={() => onRemoveToDo(todo)}>X</button>
    </li>
  );
}

export default ToDoListItem;
