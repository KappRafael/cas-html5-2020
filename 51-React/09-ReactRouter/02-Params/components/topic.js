function Topic() {
  const {topicId} = useParams();

  return (
    <div>
      <h3>Topic: {topicId}</h3>
    </div>
  );
}
