
function App() {

    return (
      <Router>
        <div>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/topics">Topics</Link>
            </li>
          </ul>

          <hr/>

          <Switch>
              <Route path="/topics/"><Topics/></Route>
              <Route path="/"> <Home/></Route>
          </Switch>
        </div>
      </Router>
    );
}
