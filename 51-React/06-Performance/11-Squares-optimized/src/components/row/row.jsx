import React, { useCallback } from "react";

import Square from "../square/square";

const Row = ({ items, onClick, index }) => {
  const onItemClick = useCallback(
    id => {
      onClick(id, index);
    },
    [onClick, index]
  );

  // console.log('Rendering Row');
  return (
    <>
      {items.map(({ id, key, clicked }) => (
        <Square key={key} onClick={onItemClick} id={id} clicked={clicked} />
      ))}
    </>
  );
};

export default React.memo(Row);
