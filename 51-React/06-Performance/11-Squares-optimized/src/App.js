import React, { useState, useCallback } from "react";

import Row from "./components/row/row";

let num = 0;

const data = Array(3000)
  .fill()
  .map((val, index) => {
    if (index % 500 === 0) {
      num = 0;
    }
    return { id: num++, key: `square-${index}` };
  });

const chunkArray = (array, chunkSize) => {
  const results = [];
  let index = 1;

  while (array.length) {
    results.push({
      items: array.splice(0, chunkSize),
      key: String(index)
    });
    index++;
  }

  return results;
};

const chunks = chunkArray(data, 500);

const App = () => {
  const [count, setCount] = useState(0);
  const [allItems, setAllItems] = useState(chunks);

  const onClick = useCallback(
    (id, index) => {
      const chunk = [...allItems[index].items];
      chunk[id].clicked = true;
      setCount(val => val + 1);
      allItems[index].items = chunk;
      setAllItems(allItems);
    },
    [allItems]
  );

  // console.log('Rendering App');
  return (
    <div>
      <p>Count: {count}</p>
      {allItems.map(({ items, key }, index) => (
        <Row items={items} onClick={onClick} key={key} index={index} />
      ))}
    </div>
  );
};

export default App;
