const {useState, useEffect, useMemo, useCallback, memo} = React;

function calculate(n) {
  const count = fibonacci(n);
  console.log('Calculated', n, count);
  return count;
}

function fibonacci(n) {
  if (n < 2)
    return 1;
  else
    return fibonacci(n - 2) + fibonacci(n - 1);
}


function App() {
  const [time, setTime] = useState(new Date());

  useEffect(() => {
      console.log('Starting Timer');
      setInterval(() => setTime(new Date()), 1000);
    }, []);

  const timeString = time.toISOString();
  const minutes = time.getMinutes();
  // const count = calculate(minutes / 4);
  const count = useMemo(() => calculate(minutes/4), [minutes]);

  const doSomething = useCallback(() => {
    alert('ALERT: ' + count);
  }, [count]);

  return (
    <div>
      <h1>Greetings from Function Component</h1>
      <p>Hello World!</p>
      <Clock time={timeString}/>
      <Minutes minutes={minutes} count={count} doSomething={doSomething}/>
    </div>
  );
}

function Clock({time}) {
  return (
    <div style={{backgroundColor: 'pink'}}>
      <div>
        {time}
      </div>

    </div>
  );
}

function Minutes2({minutes, count, doSomething}) {
  console.log('Rendering Minutes');
  return (
    <div style={{backgroundColor: 'palegreen'}}>
      <div> Current Minutes: {minutes} </div>
      <div> Count: {count} </div>
      <button onClick={doSomething}>Click Me!</button>
      <div style={{padding: 30}}>
        <Content/>
      </div>
    </div>
  );
}

const Minutes = memo(Minutes2);

function Content() {
  console.log('Rendering Content');
  return <div style={{backgroundColor: 'yellow'}}>Content</div>;
}

ReactDOM.render(
  <div>
    <App/>
  </div>,
  document.getElementById('root')
);


// DEMO:
// 1) wrap Minutes into memo
// 2) pass alert to Minutes => breaks memo
// 3) wrap alert into useCallback
// 4) use calculate(minutes) for count, then memoize count with useMemo
