import React from "react";

import "./square.css";

const Square = ({ onClick, id, clicked }) => {
  // console.log('Rendering Square');
  return (
    <div
      className={`square ${clicked && "clicked"}`}
      onClick={() => onClick(id)}
    />
  );
};

export default Square;
