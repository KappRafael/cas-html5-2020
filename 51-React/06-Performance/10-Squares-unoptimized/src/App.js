import React, { useState, useCallback } from "react";

import Square from "./components/square/square";

const data = Array(3000)
  .fill()
  .map((val, index) => {
    return { id: index, key: `square-${index}` };
  });

const App = () => {
  const [count, setCount] = useState(0);
  const [items, setItems] = useState(data);

  // console.log('Rendering App');
  return (
    <div>
      <p>Count: {count}</p>
      {items.map(({ key, id, clicked }) => (
        <Square
          key={key}
          id={id}
          clicked={clicked}
          onClick={id => {
            const newItems = [...items];

            newItems[id].clicked = true;

            setCount(val => val + 1);
            setItems(newItems);
          }}
        />
      ))}
    </div>
  );
};

export default App;
