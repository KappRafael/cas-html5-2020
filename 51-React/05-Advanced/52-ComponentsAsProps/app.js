function Advertisement() {
  return <div style={{ height: 100, backgroundColor: 'yellow' }}>Advertisement</div>;
}
function MainContent() {
  return <div style={{ height: 100, backgroundColor: 'cyan' }}>Main Content</div>;
}
function Navigation() {
  return <div style={{ height: 100, backgroundColor: 'pink' }}>Navigation</div>;
}

function Layout({ leftComponent, middleComponent, RightComponent }) {
  return (
    <div style={{ display: 'flex', justifyContent: 'space-around' }}>
      <div>{leftComponent}</div>
      <div>{middleComponent}</div>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        {[1, 2, 3].map(i => (
          <div key={i}>
            <RightComponent />
          </div>
        ))}
      </div>
    </div>
  );
}

function App() {
  const data = 42;
  return <Layout leftComponent={<Navigation />} middleComponent={<MainContent data={data} />} RightComponent={Advertisement} />;
}

ReactDOM.render(<App />, document.getElementById('root'));
