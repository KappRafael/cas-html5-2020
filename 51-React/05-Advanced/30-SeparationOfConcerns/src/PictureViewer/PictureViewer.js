import React, {useReducer} from "react";
import {stateReducer} from "./pictureViewerState";
import View from './View';
import {fetchPictureUrl} from './loadPictureApi';


function App() {

  const [state, dispatch] = useReducer(stateReducer, {imageUrl: undefined, loading: false});

  async function loadImage() {
    dispatch({type: 'LOADING_START'});
    const url = await fetchPictureUrl();
    dispatch({type: 'LOADING_SUCCESS', payload: url})
  }

  return <View loading={state.loading} imageUrl={state.imageUrl} onLoad={loadImage}/>

}

export default App;
