import { makeAutoObservable } from 'mobx';


interface ToDo {
    id: number;
    title: string;
}

class ToDoStore {
    todos: ToDo[] = [];
    constructor() {
        makeAutoObservable(this);

        // setInterval(() => {
        //     this.todos.push({id: Math.random(), title: new Date().toISOString()})
        //     console.log('Todos', this.todos);
        // }, 1000)
    }

    async loadToDos (){
        const todoResponse = await fetch('http://localhost:3456/todos')
        const todos = await todoResponse.json();
        this.todos = todos.result;
    }
}

const todoStore = new ToDoStore();

export {todoStore};
