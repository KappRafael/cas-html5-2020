import React from 'react';
import logo from './logo.svg';
import './App.css';
import {todoStore} from './todoStore';
import { observer } from 'mobx-react-lite';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

        <button onClick={()=> todoStore.loadToDos()}>Load</button>

        <ul>
        {
          todoStore.todos.map(todo => <li key={todo.id}>{todo.title}</li>)
        }
        </ul>

      </header>
    </div>
  );
}

export default observer(App);
