const { autorun, makeAutoObservable, observable} = mobx;

class Person {
  firstName = 'Tyler';
  lastName = 'Doe';
  constructor() {
    makeAutoObservable(this);
  }
  get fullName(){
    return `${this.firstName} ${this.lastName}`;
  }
}

var person = new Person();


var message = observable({
    text: 'Observing the observable!'
  }
);

const render = () => {
  document.body.innerText =
    person.fullName +
    ' : ' +
    message.text;
};

autorun(render);
