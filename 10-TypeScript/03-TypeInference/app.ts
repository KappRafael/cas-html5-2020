var a = [{ name: 'Tyler'}, { name: 'Durden'}];
var sorted = a.slice(0);
sorted.sort(function (x,y){
    return x.name.localeCompare(y.name);
});
document.body.innerHTML = sorted[0].name;