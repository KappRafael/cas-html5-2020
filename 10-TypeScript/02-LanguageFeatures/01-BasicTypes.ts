{
    let b: boolean = true;

    let n: number = 42;

    let s: string = 'Hello!';

    let a1: number[] = [1, 2, 3];
    let a2: Array<string> = ['Hello', 'World', '!'];

    enum ArrowDirection { Up, Down, Left, Right}

    let e: ArrowDirection = ArrowDirection.Up;


    let x: string = 'Hello World';
    x = 'Hello Universe';
// x = 42; // => compiler error (even when you omit the type annotation)
}