
async function testAsync(): Promise<void> {
  
  try {
    console.log('Going to wait ...');
    await delayAsync(1000);

    console.log('Finished waiting ...');
    console.log('Waiting another turn ...');
    await delayAsync(1000);
    
    //await errorAsync(1000);
    console.log('The wait is over!');
  }
  catch (e){
    console.log('Caught the error!');
  }
}

console.log('I am starting ...');
testAsync();
console.log('I am not blocking!');


function delayAsync(timeout: number): Promise<void> {
  return new Promise<void>((resolve) => setTimeout(resolve, timeout));
}

function errorAsync(timeout: number): Promise<void> {
  return new Promise<void>((resolve, reject) => setTimeout(reject, timeout));
}