import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'aw-rederless-directive-parent',
  template: `
    <h3>Toggle Directive</h3>
    <div *awToggle="let controller; on: false">
      <button (click)="controller.setOn()">Blue pill</button>
      <button (click)="controller.setOff()">Red pill</button>

      <div>
        <span *ngIf="controller.on">Out of the Matrix!</span>
        <span *ngIf="!controller.on">Still inside the Matrix!</span>
      </div>
    </div>
  `,
  styleUrls: ['./renderless-directive-parent.component.scss']
})
export class RenderlessDirectiveParentComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
