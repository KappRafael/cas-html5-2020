import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InjectionParentComponent } from './01-parent-injection/injection-parent.component';
import { CompoundParentComponent } from './02-compound-component/compound-parent.component';
import { TemplateParentComponent } from './03-ng-templates/template-parent.component';
import { TemplateRefParentComponent } from './04-template-ref/template-ref-parent.component';
import { DynamicCreationParentComponent } from './05-dynamic-component/dynamic-creation-parent.component';
import { RenderlessExportAsParentComponent } from './06-renderless-exportas/renderless-export-as-parent/renderless-export-as-parent.component';
import { RenderlessDirectiveParentComponent } from './07-renderless-directive/renderless-directive-parent/renderless-directive-parent.component';

const routes: Routes = [
  { path: 'parent-injection', component: InjectionParentComponent },
  { path: 'compound-component', component: CompoundParentComponent },
  { path: 'ng-templates', component: TemplateParentComponent },
  { path: 'template-property', component: TemplateRefParentComponent },
  { path: 'dynamic-component', component: DynamicCreationParentComponent },
  { path: 'renderless-exportas', component: RenderlessExportAsParentComponent },
  { path: 'renderless-directive', component: RenderlessDirectiveParentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentPatternsRoutingModule {}
