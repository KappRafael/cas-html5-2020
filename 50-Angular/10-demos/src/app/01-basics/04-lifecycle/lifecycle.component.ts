import {
  OnChanges,
  SimpleChange,
  OnInit,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy
} from '@angular/core';
import { Component, Input } from '@angular/core';

let nextId = 1;

@Component({
  selector: 'aw-lifecycle',
  template: '<p>Current date {{date}}</p>'
})
export class LifecycleComponent
  implements
    OnChanges,
    OnInit,
    DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy {
  @Input() date: Date | undefined;

  private verb = 'initialized';

  constructor() {
    const is = this.date ? 'is' : 'is not';
    this.logIt(`date ${is} known at construction`);
  }

  protected logIt(msg: string): void {
    const message = `#${nextId++} ${msg}`;
    console.log(message);
  }

  ngOnInit(): void {
    this.logIt(`OnInit`);
  }

  // only called for/if there is an @input variable set by parent.
  ngOnChanges(changes: { [propertyName: string]: SimpleChange }): void {
    const changesMsgs: string[] = [];
    for (const propName in changes) {
      if (propName === 'name') {
        // tslint:disable-next-line
        const name = changes['name'].currentValue;
        changesMsgs.push(`name ${this.verb} to "${name}"`);
      } else {
        changesMsgs.push(propName + ' ' + this.verb);
      }
    }
    this.logIt(`OnChanges: ${changesMsgs.join('; ')}`);
    this.verb = 'changed'; // next time it will be a change
  }

  // Beware! Called frequently!
  // Called in every change detection cycle anywhere on the page
  ngDoCheck(): void {
    this.logIt(`DoCheck`);
  }

  ngAfterContentInit(): void {
    this.logIt(`AfterContentInit`);
  }

  // Beware! Called frequently!
  // Called in every change detection cycle anywhere on the page
  ngAfterContentChecked(): void {
    this.logIt(`AfterContentChecked`);
  }

  ngAfterViewInit(): void {
    this.logIt(`AfterViewInit`);
  }

  // Beware! Called frequently!
  // Called in every change detection cycle anywhere on the page
  ngAfterViewChecked(): void {
    this.logIt(`AfterViewChecked`);
  }

  ngOnDestroy(): void {
    this.logIt(`OnDestroy`);
  }
}
