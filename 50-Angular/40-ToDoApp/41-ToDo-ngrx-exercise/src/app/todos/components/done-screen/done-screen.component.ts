import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ToDo } from '../../model/todos/todo.model';
import { IToDosState } from '../../model/todos/todo.store';
import { Store } from '@ngrx/store';
import { getDoneToDos } from '../../model/todos/todo.selectors';
import { loadRequest } from '../../model/todos/todo.actions';

@Component({
  selector: 'td-done-todos',
  template: `
      <h4>You are so productive:</h4>
      <div class="link-container">
          <a routerLink="/">... back to work!</a>
      </div>

      <section class="main">
          <td-todo-list [todos]="doneTodos$ | async" (removeToDo)="removeToDo($event)"></td-todo-list>
      </section>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DoneScreenComponent implements OnInit {

  doneTodos$: Observable<ToDo[]>;

  constructor(private store: Store<{ todos: IToDosState }>) {
    this.doneTodos$ = this.store.select(getDoneToDos);
  }

  ngOnInit(): void {
    this.store.dispatch(loadRequest());
  }

  removeToDo(todo: ToDo) {
    // TODO: Exercise!
  }

}
