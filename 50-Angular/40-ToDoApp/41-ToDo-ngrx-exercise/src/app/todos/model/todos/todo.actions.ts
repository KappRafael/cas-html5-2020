import { createAction, props } from '@ngrx/store';
import {ToDo} from './todo.model';

export const loadRequest = createAction('[ToDos] Load ToDos');
export const loadSuccess = createAction('[ToDos] Load ToDos Success', props<{todos: ToDo[]}>());
export const loadError = createAction('[ToDos] Load ToDos error');


export const createRequest = createAction('[ToDos] Create ToDo', props<{todo: ToDo}>());
export const createSuccess = createAction('[ToDos] Create ToDo Success', props<{todo: ToDo}>());
export const createError = createAction('[ToDos] Create ToDo error');

export const completeRequest = createAction('[ToDos] Complete ToDo', props<{todo: ToDo}>());
export const completeSuccess = createAction('[ToDos] Complete ToDo Success');
export const completeError = createAction('[ToDos] Complete ToDo error');

