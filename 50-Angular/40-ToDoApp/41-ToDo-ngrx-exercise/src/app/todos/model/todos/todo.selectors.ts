import {createFeatureSelector, createSelector} from '@ngrx/store';
import {IToDosState} from './todo.store';

const getToDosState = createFeatureSelector('todos');
export const getPendingToDos = createSelector(getToDosState, (state: IToDosState) => state.pendingTodos);
export const getDoneToDos = createSelector(getToDosState, (state: IToDosState) => state.doneTodos);
