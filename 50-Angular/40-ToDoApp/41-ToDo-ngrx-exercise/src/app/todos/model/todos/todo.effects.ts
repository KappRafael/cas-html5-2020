import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import {
  completeError,
  completeRequest,
  completeSuccess,
  createError,
  createRequest,
  createSuccess,
  loadError,
  loadRequest,
  loadSuccess,
} from './todo.actions';
import { Store } from '@ngrx/store';
import { IToDosState } from './todo.store';
import { ToDoApiService } from '../../api/todo-api.service';
import { catchError, concatMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { showMessage } from '../messages/message.actions';


@Injectable()
export class ToDoEffects {

  loadToDos$ = createEffect(() => this.actions$
    .pipe(
      ofType(loadRequest),
      tap(() => this.store.dispatch(showMessage({message: 'Loading ...'}))),
      switchMap(
        () => this.toDoService.getTodos()
          .pipe(
            map(todos => loadSuccess({todos})),
            catchError(error => of(loadError()))
          )
      )
    )
  );

  createToDo$ = createEffect(() => this.actions$
    .pipe(
      ofType(createRequest),
      tap(() => this.store.dispatch(showMessage({message: 'Saving ...'}))),
      concatMap(
        action => this.toDoService.saveTodo(action.todo)
          .pipe(
            map(savedToDo => (createSuccess({todo: savedToDo}))),
            catchError(error => of(createError()))
          )
      )
    )
  );

  completeToDo$ = createEffect(() => this.actions$
    .pipe(
      ofType(completeRequest),
      tap(() => this.store.dispatch(showMessage({message: 'Saving ...'}))),
      concatMap(
        action => this.toDoService.updateTodo({...action.todo, completed: true})
          .pipe(
            map(savedToDo => (completeSuccess())),
            catchError(error => of(completeError()))
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private toDoService: ToDoApiService,
    private store: Store<IToDosState>) {
  }
}
