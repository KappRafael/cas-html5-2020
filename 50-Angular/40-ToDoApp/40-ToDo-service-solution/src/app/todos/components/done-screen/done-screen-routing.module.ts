import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DoneScreenComponent} from './done-screen.component';


const appRoutes: Routes = [

    // { path: 'done', component: DoneTodosComponent},
    { path: '', component: DoneScreenComponent},
];

export const routing: ModuleWithProviders = RouterModule.forChild(appRoutes);
