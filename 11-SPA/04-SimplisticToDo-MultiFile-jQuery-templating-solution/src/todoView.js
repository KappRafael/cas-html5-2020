(function(global) {
    'use strict';

    // Getting a reference to a controller like this does not work, since the corresponding file only gets loadet later:
    //var controller = global.todo.controller;

    function TodoView() {

        var self = this;
        var $self = $(self);

        var todoInputElem =  $('#input');
        var addBtn = $('#addBtn');

        // Note there is a problem:
        // itemChanged rebuilds the DOM so the click event is never triggered since the button got thrown away

        todoInputElem.on('blur', function(){
            $self.trigger('itemChanged', todoInputElem.val());
        });

        addBtn.on('click', function(){
            $self.trigger('itemAdded');
        });

        self.render = function(todo){
            todoInputElem.val(todo.text);
        }
    }


    global.todo = global.todo || {};
    global.todo.TodoView = TodoView;

})(window);
