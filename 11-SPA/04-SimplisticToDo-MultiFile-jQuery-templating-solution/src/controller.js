(function (global) {
    'use strict';

    var controller = {

        init: function () {

            var model = global.todo.createModel();
            var summaryView = new global.todo.SummaryView();
            var mainView = new global.todo.MainView();

            $(model).on('changed', updateUi);

            $(mainView).on('itemChanged', function (event, text) { updateTodo(text) } );
            $(mainView).on('itemAdded', addItem);
            $(mainView).on('itemRemoved', function (event, index) {removeItemAtIndex(index)} );

            summaryView.render(model.getTodoList());
            mainView.render(model);

            function updateTodo(todoText) {
                model.updateNewTodo(todoText);
            }
            function addItem() {
                model.addNewTodo();
            }
            function removeItemAtIndex(index) {
                model.removeTodoAtIndex(index);
            }

            function updateUi() {

                if (location.hash === '' || location.hash === '#main'){
                    mainView.render(model);
                }
                else if (location.hash === '#summary'){
                    summaryView.render(model.getTodoList());
                }

            }

            return updateUi;

        }
    };

    global.todo = global.todo || {};
    global.todo.controller = controller;

})(window);
