var expect    = require("chai").expect;

global.$ = require('jquery');

require('../../src/model');

describe("ToDo List", function() {

    var model;

    beforeEach(function() {
        model = window.todo.createModel();
    });

    it("should update new todo", function(){
        model.updateNewTodo('test');
        expect(model.getNewTodo().text).to.equal('test');
    });

    it("should add a new todo", function(){
        model.addNewTodo();
        expect(model.getTodoList().length).to.equal(1);
    });

    it("should remove a new todo", function(){
        model.addNewTodo();
        model.removeTodoAtIndex(0);
        expect(model.getTodoList().length).to.equal(0);
    })
});
