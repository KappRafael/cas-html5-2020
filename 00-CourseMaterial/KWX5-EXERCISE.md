#  State Management Exercises

## Exercise 1: Redux Basics

Inspect the project `60-Redux/12-redux-state-exercise/`.  
The project implements a redux store without a UI.

You can execute a test which triggers the store functionality like this:

	npm install
	npm test

Extend the store, so todo items can be marked as completed.



## Exercise 2: React ToDo App with Redux

In the project `51-React/40-ToDoApp/23-todo-redux-starter`: Implement the functionality to delete a ToDo item from the store.  

Inspect the solutions at  `51-React/40-ToDoApp/23-todo-redux-solution`





## Exercise 3: Angular Modularization

Inspect the project `50-Angular/40-ToDoApp/21-Simple-ToDo-modularized-solution`.  

Run the project with:

	npm install
	npm start

Inspect the different Angular modules, their routing setup and the lazy loading.



## Exercise 4: Angular with a simple service

Inspect the project `50-Angular/40-ToDoApp/40-ToDo-service-solution`.  

Run the project with:

	npm install
	npm start

Inspect  the service and how it's state is accessed from the components.



## Exercise 5: Angular with NgRx

Inspect the project `50-Angular/40-ToDoApp/41-ToDo-ngrx-exercise`.  

Implement the functionality to delete a ToDo item from the store.

Inspect the solution at `50-Angular/40-ToDoApp/41-ToDo-ngrx-solution`. 