## Exercise 1: Three Apps

Compare the three ToDo-applications at the following urls:

- [https://spa-demos.herokuapp.com/todo1/](https://spa-demos.herokuapp.com/todo1/)
- [https://spa-demos.herokuapp.com/todo2/](https://spa-demos.herokuapp.com/todo2/)
- [https://spa-demos.herokuapp.com/todo3/](https://spa-demos.herokuapp.com/todo3/)

What can you find out about their technical implementation?  
Write down all the differences.

(The source code for the three examples is in `01-Intro/00-ToDo-Apps`)



## Exercise 2 (optional): A modern Front-End Build

- Change into the directory `01-Intro/05-SimplisticToDoModern`
- Execute the steps of the frontend build as described in the `README.md`
- Run the app in development mode, try to debug it
- Inspect the result of the build in the directory `public`. 
- Run the tests through npm-scripts (i.e. `npm run test`) and optionally with IDE integration
- Implement the 'done' functionality (inspect the commented code) and extend the tests for this functionality