

## Di 16. März 16:30 - MarbleCollector ✅

Aeschlimann David, Hofer Severin, Rindisbacher Marcel, Schenk Jan



## Di 16. März 17:30 - Multicard

Fankhauser Frank, Lerch Martin



## Di 16. März 18:30 - Waschplan

Jakob Marco, Atesci Remzi, Egli Michael



## Do 18. März 9:00 - Toucher ✅

Balmer Nicolas, Briggen Roger, Hegg Fabian, Kurowski Simon



## Do 18. März 13:00 - Destination Finder

Schmid Lorenz, Dumermuth Nicolas, Bürgi Christian



## Di 23. März 17:30 -  Rob

Jüni Philipp, Dietrich Simon, Schneider Felix



## Di 23. März 18:30 - Workoutside

Bleuler Mira, Gelati Marco, Kapp Rafael

---

---



Ich würde für das Review 30 - 45 min vorsehen. Falls es länger als 1h gehen würde, müssten wir einen weiteren Termin vereinbaren.

Ich könnte folgende weitere Termine offerieren, falls ein Termin nicht passt oder ein anderer Termin genehmer ist:

- Do 18.März 8:00 - 16:00
- Mi 17. März 8:00 - 18:00



