/*
 This lesson uses the lodash library: https://lodash.com/
 Lodashes utility functions exposed by the _ variable
 */
lesson('About Higher Order Functions', function () {

    learn('how to use filter to return array items that meet a criteria', function () {
        var numbers = [1,2,3];
        var odd = numbers.filter(function (x) { return x % 2 !== 0; });

        expect(odd).toEqual(FILL_ME_IN);
        expect(odd.length).toBe(FILL_ME_IN);
        expect(numbers.length).toBe(FILL_ME_IN);
    });

    learn('how to use find to search an array', function () {
        var numbers = [1,2,3];

        expect(numbers.find(function (x) { return x % 2 !== 0; })).toEqual(FILL_ME_IN);
        expect(numbers.find(function (x) { return x % 2 === 0; })).toEqual(FILL_ME_IN);
        expect(numbers.find(function (x) { return x === 2; })).toEqual(FILL_ME_IN);
        expect(numbers.findIndex(function (x) { return x === 2; })).toEqual(FILL_ME_IN);
        expect(numbers.findIndex(function (x) { return x % 2 !== 0; })).toEqual(FILL_ME_IN);
    });

    learn('how to use \'map\' to transform each element', function () {
        var numbers = [1, 2, 3];
        var numbersPlus1 = numbers.map(function(x) { return x + 1; });

        expect(numbersPlus1).toEqual(FILL_ME_IN);
        expect(numbers).toEqual(FILL_ME_IN);
    });

    learn('how to use \'reduce\' to update the same result on each iteration', function () {
        var numbers = [1, 2, 3];
        var reducer = function(/* result from last call */ memo, /* current */ x) { return memo + x; };
        var reduction = numbers.reduce(reducer, /* initial */ 0);

        expect(reduction).toBe(FILL_ME_IN);
        expect(numbers).toEqual(FILL_ME_IN);
    });

    learn('how to use \'forEach\' for simple iteration', function () {
        var numbers = [1,2,3];
        var msg = '';
        var isEven = function (item) {
            msg += (item % 2) === 0;
        };

        numbers.forEach(isEven);

        expect(msg).toEqual(FILL_ME_IN);
        expect(numbers).toEqual(FILL_ME_IN);
    });

    learn('about using \'every\' to test whether all items pass condition', function () {
        var onlyEven = [2,4,6];
        var mixedBag = [2,4,5,6];

        var isEven = function(x) { return x % 2 === 0; };

        expect(onlyEven.every(isEven)).toBe(FILL_ME_IN);
        expect(mixedBag.every(isEven)).toBe(FILL_ME_IN);
    });

    learn('about using \'some\' to test if any items passes condition' , function () {
        var onlyEven = [2,4,6];
        var mixedBag = [2,4,5,6];

        var isEven = function(x) { return x % 2 === 0; };

        expect(onlyEven.some(isEven)).toBe(FILL_ME_IN);
        expect(mixedBag.some(isEven)).toBe(FILL_ME_IN);
    });
});

