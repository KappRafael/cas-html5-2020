var jsLessons;
(function (jsLessons) {

    jsLessons.lessonFiles = [
        '00-CourseSelection.js',
        '01-Basics.js',
        '02-Variables.js',
        '03-Strings.js',
        '04-Arrays.js',
        '05-Objects.js',
        '06-Classes.js',
        '07-Functions.js',
        '08-HigherOrderFunctions.js',
        '11-Destructuring.js',
        '12-Spread.js',
        '21-LegacyClassesAndNamespaces.js',
        '22-LegacyInheritance.js',
        '23-LegacyPatterns.js',
        '99-BadParts.js'
    ];

    jsLessons.start();


})(jsLessons || (jsLessons = {}));
