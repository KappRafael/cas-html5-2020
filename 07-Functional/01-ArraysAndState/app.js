console.log('Running example ...');

////////////////
// DEMOS
///////////////

const a = [1, 2, 3, 4, 5, 6, 7, 8, 9];

a.push('a', 'b');
console.log('Push', a);

a.splice(3, 2);
console.log('Splice', a);

const added = a.concat('c', 'd');
console.log('Added', added);
// Note: Alternative with array spread:
// const added = [...a, 'c', 'd'];

const filtered = a.filter(e => e % 2 === 0);
console.log('Filtered', filtered);

const mapped = a.map(e => `https://swapi.co/api/people/${e}`);
console.log('Mapped', mapped);

const sentences = ['the fox jumps over the dog', 'the early bird gets the worm', 'this is great'];
const words = sentences.flatMap(e => e.split(' ')); // ES2018!
console.log('Words', words);

const n = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const sum = n.reduce((acc, e) => acc + e, 0);
console.log('Sum', sum);

const stats = n.reduce(
  (acc, e) => {
    const newCount = acc.count + 1;
    const newSum = acc.sum + e;
    return {
      count: newCount,
      sum: newSum,
      average: newSum / newCount
    };
  },
  { count: 0, sum: 0, average: 0 }
);
console.log('Stats', stats);

////////////////
// EXERCISES
///////////////

const todos = [
  { id: 1, title: 'Learn JavaScript' },
  { id: 2, title: 'Learn TypeScript' },
  { id: 3, title: 'Learn React' },
  { id: 4, title: 'Learn Angular' }
];

const newToDo = { id: 5, title: 'Learn Vue.js' };

// Task: Add the newToDo to the todos by mutating the array

// Task: Add the newToDo to the todos by creating a new array

// Task: Remove the ToDo with id '3' from the todos by mutating the array

// Task: Remove the ToDo with id '3' from the todos by creating a new array

// Task: Change the title of the ToDo with id '3' by mutating the object

// Task: Change the title of the ToDo with id '3' by creating a new array that contains a new object

// Task: Implement a function `function myMap(array, mappingFunc)` that implements the behavior of `map` by using `array.reduce`
const a2 = [1, 2, 3, 4, 5, 6, 7, 8, 9];
function myMap(a, cb) {
  return a;
} // TODO
const myMapped = myMap(a2, e => `https://swapi.co/api/people/${e}`);
console.log('My Mapped', myMapped);

// Task: Implement a function `function myFlatMap(array, mappingFunc)` that implements the behavior of `flatMap` by using `array.reduce`
const sentences2 = ['the fox jumps over the dog', 'the early bird gets the worm', 'this is great'];
function myFlatMap(a, cb) {
  return a;
} // TODO
const myFlatMapped = myFlatMap(sentences2, e => e.split(' '));
console.log('My Flat Mapped', myFlatMapped);

// Task: Implement the reducer function below so that it can handle the "actions" of adding and removing a ToDo
function stateReduce(todos, action) {
  // TODO
  return [...todos];
}

const initialState = [
  { id: 1, title: 'Learn JavaScript' },
  { id: 2, title: 'Learn TypeScript' },
  { id: 3, title: 'Learn React' },
  { id: 4, title: 'Learn Angular' }
];

const addAction = { type: 'ADD', payload: { id: 42, title: 'Learn Redux' } };
const removeAction = { type: 'REMOVE', payload: { id: 4 } };

const newState1 = stateReduce(initialState, addAction);
console.log('Added State', newState1); // should contain 'Learn Redux'

const newState2 = stateReduce(newState1, removeAction);
console.log('Removed State', newState2); // should not contain 'Learn Angular'

// Task: Immutable State: crate a new version of the `nestedObject` so that Robert Paulson gets the new fighting skill 'wrestling'
// 1: use the spread operator
// 2: use immerjs (https://immerjs.github.io/)
const nestedObject = {
  firstName: 'Tyler',
  lastName: 'Durden',
  deciples: [
    { id: 1, firstName: 'Edward', lastName: 'Norton', fightingSkills: [] },
    {
      id: 2,
      firstName: 'Robert',
      lastName: 'Paulson',
      fightingSkills: ['boxing', 'sumo']
    }
  ]
};

const changedNestedObject = /*TODO*/ nestedObject;
console.log('Is same object?', changedNestedObject === nestedObject); // should be false
console.log('Can wrestle?', changedNestedObject.deciples[0].fightingSkills.includes('wresting')); // should be true
