console.log('Running example ...');

////////////////
// EXERCISES
///////////////

const todos = [
  { id: 1, title: 'Learn JavaScript' },
  { id: 2, title: 'Learn TypeScript' },
  { id: 3, title: 'Learn React' },
  { id: 4, title: 'Learn Angular' }
];

const newToDo = { id: 5, title: 'Learn Vue.js' };

// Task: Add the newToDo to the todos by mutating the array
todos.push(newToDo);
console.log('Added', todos);

// Task: Add the newToDo to the todos by creating a new array
// const addedTodos = todos.concat(newToDo);
const addedTodos = [...todos, newToDo];
console.log('Added', addedTodos);

// Task: Remove the ToDo with id '3' from the todos by mutating the array
todos.splice(
  todos.findIndex(e => e.id === 3),
  1
);
console.log('Removed', todos);

// Task: Remove the ToDo with id '2' from the todos by creating a new array
const removedTodos = todos.filter(e => e.id !== 2);
console.log('Removed', removedTodos);

// Task: Change the title of the ToDo with id '4' by mutating the object
todos.find(t => t.id === 4).title = 'Learn functional programming';
console.log('Changed', todos);

// Task: Change the title of the ToDo with id '4' by creating a new array that contains a new object
const changedTodos = todos.map(t => (t.id === 4 ? { ...t, title: 'Learn OO programming' } : t));
console.log('Changed', changedTodos);

// Task: Implement a function `function myMap(array, mappingFunc)` that implements the behavior of `map` by using `array.reduce`
const a2 = [1, 2, 3, 4, 5, 6, 7, 8, 9];

function myMap(a, cb) {
  return a.reduce((acc, e) => {
    acc.push(cb(e));
    return acc;
  }, []);
}

const myMapped = myMap(a2, e => `https://swapi.co/api/people/${e}`);
console.log('My Mapped', myMapped);

// Task: Implement a function `function myFlatMap(array, mappingFunc)` that implements the behavior of `flatMap` by using `array.reduce`
const sentences2 = ['the fox jumps over the dog', 'the early bird gets the worm', 'this is great'];

function myFlatMap(a, cb) {
  return a.reduce((acc, e) => {
    acc.push(...cb(e));
    return acc;
  }, []);
}

const myFlatMapped = myFlatMap(sentences2, e => e.split(' '));
console.log('My Flat Mapped', myFlatMapped);

// Task: Implement the reducer function below so that it can handle the "actions" of adding and removing a ToDo
function stateReduce(todos, action) {
  switch (action.type) {
    case 'ADD':
      return [...todos, action.payload];
    case 'REMOVE':
      return todos.filter(t => t.id !== action.payload.id);
    default:
      return [...todos];
  }
}

const initialState = [
  { id: 1, title: 'Learn JavaScript' },
  { id: 2, title: 'Learn TypeScript' },
  { id: 3, title: 'Learn React' },
  { id: 4, title: 'Learn Angular' }
];

const addAction = { type: 'ADD', payload: { id: 42, title: 'Learn Redux' } };
const removeAction = { type: 'REMOVE', payload: { id: 4 } };

const newState1 = stateReduce(initialState, addAction);
console.log('Added State', newState1); // should contain 'Learn Redux'

const newState2 = stateReduce(newState1, removeAction);
console.log('Removed State', newState2); // should not contain 'Learn Angular'

// Task: Immutable State: crate a new version of the `nestedObject` so that Robert Paulson gets the new fighting skill 'wrestling'
// 1: use the spread operator
// 2: use immerjs (https://immerjs.github.io/)

const nestedObject = {
  firstName: 'Tyler',
  lastName: 'Durden',
  deciples: [
    { id: 1, firstName: 'Edward', lastName: 'Norton', fightingSkills: [] },
    {
      id: 2,
      firstName: 'Robert',
      lastName: 'Paulson',
      fightingSkills: ['boxing', 'sumo']
    }
  ]
};

const changedNestedObject1 = {
  ...nestedObject,
  deciples: nestedObject.deciples.map(d => (d.id !== 2 ? d : { ...d, fightingSkills: [d.fightingSkills, 'wrestling'] }))
};
console.log('Is same object?', changedNestedObject1 === nestedObject); // should be false
console.log('Can wrestle?', changedNestedObject1.deciples[1].fightingSkills.includes('wrestling')); // should be true

const { produce } = immer;
const changedNestedObject2 = produce(nestedObject, draft => {
  draft.deciples[1].fightingSkills.push('wrestling');
});
console.log('Is same object?', changedNestedObject2 === nestedObject); // should be false
console.log('Can wrestle?', changedNestedObject2.deciples[1].fightingSkills.includes('wrestling')); // should be true
